//
//  ConnectionRemoteService.swift
//  DemoPedido
//
//  Created by Alejandro Arce on 20/4/17.
//  Copyright © 2017 ArceGuerra, Alejandro. All rights reserved.
//

import Foundation


import Alamofire
import SwiftyJSON


class ConnectionRemoteService{
    
 //let urlBase = "http://77.240.114.43/SMC/api"
 let urlBase = "http://tienda.conway.es/SMC/api"
    
    
    
    func clonarPedido(usuario: String, numpedido: String, numpedidoweb: String, completionHandler: @escaping (String?) -> Void){
        
        var arrayParametros = Parameters ()
        let url = URL(string:  urlBase + "/clonarpedido/")!
        
        let numpedidoTrim = numpedido.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        arrayParametros.updateValue(usuario, forKey: "usuario")
        arrayParametros.updateValue(numpedidoTrim, forKey: "numpedido")
        arrayParametros.updateValue(numpedidoweb, forKey: "numpedidoweb")

        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json"
        ]
        
        Alamofire.request(url, method: .post, parameters: arrayParametros, encoding: JSONEncoding.default, headers: headers).validate().responseJSON() { response in
            
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    let contr = json["codigoCtr"].stringValue
                    if contr == "1"{
                        completionHandler (json["NumPedidoWeb"].stringValue)
                    }else{
                        completionHandler("NOADD")
                        
                    }
                    
                }
            case .failure(let error):
                print(error)
                completionHandler(nil)
                
            }
            
            
        }
        
        
        
        
        
    }
    

    
    
    
    
    
    func deletePedido(usuario: String, numpedidoweb: String, completionHandler: @escaping (String?) -> Void){
        
        var arrayParametros = Parameters ()
        let url = URL(string:  urlBase + "/borrarpedido/")!
        
        arrayParametros.updateValue(usuario, forKey: "usuario")
        arrayParametros.updateValue(numpedidoweb, forKey: "numpedidoweb")
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json"
        ]
        
        Alamofire.request(url, method: .post, parameters: arrayParametros, encoding: JSONEncoding.default, headers: headers).validate().responseJSON() { response in
            
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    let contr = json["codigoCtr"].stringValue
                    if contr == "1"{
                        completionHandler (contr)
                    }else{
                        completionHandler("NODEL")
                        
                    }
                    
                }
            case .failure(let error):
                print(error)
                completionHandler(nil)
                
            }
            
            
        }
        
        
        
        
        
    }

    
    
    
    
    
    func lockPedido(usuario: String, numpedidoweb: String, completionHandler: @escaping (String?) -> Void){
        
        var arrayParametros = Parameters ()
        let url = URL(string:  urlBase + "/cerrarpedido/")!
        
        arrayParametros.updateValue(usuario, forKey: "usuario")
        arrayParametros.updateValue(numpedidoweb, forKey: "numpedidoweb")
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json"
        ]
        
        Alamofire.request(url, method: .post, parameters: arrayParametros, encoding: JSONEncoding.default, headers: headers).validate().responseJSON() { response in
            
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    let contr = json["codigoCtr"].stringValue
                    if contr == "1"{
                        completionHandler (contr)
                    }else{
                        completionHandler("NODEL")
                        
                    }
                    
                }
            case .failure(let error):
                print(error)
                completionHandler(nil)
                
            }
            
            
        }
        
        
        
        
        
    }

    
    
    
    
    
    func nuevoPedido(usuario: String, numpedido: String, completionHandler: @escaping (String?) -> Void){
        
        var arrayParametros = Parameters ()
        let url = URL(string:  urlBase + "/nuevopedido/")!
        
        let numpedidoTrim = numpedido.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)

        
        arrayParametros.updateValue(usuario, forKey: "usuario")
        arrayParametros.updateValue(numpedidoTrim, forKey: "numpedido")
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json"
        ]
        
        Alamofire.request(url, method: .post, parameters: arrayParametros, encoding: JSONEncoding.default, headers: headers).validate().responseJSON() { response in
            
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    let contr = json["codigoCtr"].stringValue
                    if contr == "1"{
                         completionHandler (json["NumPedidoWeb"].stringValue)
                    }else{
                        completionHandler("NOADD")

                    }
                    
                }
            case .failure(let error):
                print(error)
                completionHandler(nil)
                
            }
            
            
        }

        
        
        
       
    }
    
    
    
    
    func upLoadOrderDetail (usuario:String,
                         numpedidoweb:String,
                         cod_articulo:String,
                         cantidad:String,
                         num_linea:String,
                         accion:String, completionHandler: @escaping (UpdateDetailResult?) -> Void){
        
        var arrayParametros = Parameters ()
        let url = URL(string:  urlBase + "/subirdetallepedido/")!
        
        arrayParametros.updateValue(usuario, forKey: "usuario")
        arrayParametros.updateValue(numpedidoweb, forKey: "numpedidoweb")
        arrayParametros.updateValue(cod_articulo, forKey: "cod_articulo")
        arrayParametros.updateValue(cantidad, forKey: "cantidad")
        if accion == "update" || accion == "delete" {
            arrayParametros.updateValue(num_linea, forKey: "num_linea")
        }
        arrayParametros.updateValue(accion, forKey: "accion")
    
        let headers: HTTPHeaders = [
            "Content-Type": "application/json"
        ]
        
        Alamofire.request(url, method: .post, parameters: arrayParametros, encoding: JSONEncoding.default, headers: headers).validate().responseJSON() { response in
            
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    let resul = UpdateDetailResult.init(codigo_ctr: json["codigoCtr"].stringValue, num_lin: json["num_linea"].stringValue)
                    completionHandler(resul)
                    
                }
            case .failure(let error):
                print(error)
                completionHandler(nil)
                
            }
            
            
        }
    
    
    }
    
    
    
    
    
    
    
    
    
    
    func buscarArticulo (usuario:String,
                        cod_articulo:String,
                        ref_art_cliente:String,
                        cod_familia:String,
                        codigo_EAN_DUN:String,
                        des_articulo:String,
                        novedad:String, completionHandler: @escaping ([Article]?) -> Void){
   
        var arrayParametros = Parameters ()
        let url = URL(string:  urlBase + "/buscararticulo/")!
        
//        arrayParametros = ["usuario":usuario,
//                           "cod_articulo": cod_articulo,
//                           "ref_art_cliente": ref_art_cliente,
//                           "cod_familia" : cod_familia,
//                           "codigo_EAN_DUN":codigo_EAN_DUN,
//                           "des_articulo":des_articulo,
//                           "novedad":novedad]
        
         arrayParametros.updateValue(usuario, forKey: "usuario")
        if cod_articulo != ""{
            arrayParametros.updateValue(cod_articulo, forKey: "cod_articulo")
        }
        if ref_art_cliente != ""{
            arrayParametros.updateValue(ref_art_cliente, forKey: "ref_art_cliente")
        }
        if cod_familia != ""{
            arrayParametros.updateValue(cod_familia, forKey: "cod_familia")
        }
        if des_articulo != ""{
            arrayParametros.updateValue(des_articulo, forKey: "desc_articulo")
        }
        if  codigo_EAN_DUN != ""{
            arrayParametros.updateValue(codigo_EAN_DUN, forKey: "codigo_EAN_DUN")
        }else{
             arrayParametros.updateValue(novedad, forKey: "novedad")
        }

        
//        arrayParametros.updateValue(novedad, forKey: "novedad")

//
//        if let us = usuario { arrayParametros
        
//        arrayParametros = ["usuario":usuario,
//                           "Cod_Articulo": "",
//                           "desc_articulo": des_articulo,
//                        "novedad":"0"]

        
        
        Alamofire.request(url, method: .get, parameters: arrayParametros, encoding: URLEncoding.default, headers: nil).validate().responseJSON() { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    var result = [Article]()
                    let control = json["codigoCtr"].stringValue
                    let datos = json["Articulos"].arrayValue
                    if control  == "0" {
                        completionHandler(result)
                    }
                    for dato in datos {
                        let article = Article(des_articulo: dato["desc_articulo"].stringValue,
                                                  cod_articulo: dato["cod_articulo"].stringValue,
                                                  pack: dato["pack"].stringValue,
                                                  precio_pack: dato["precio_pack"].stringValue,
                                                  peso: dato["peso"].stringValue,
                                                  novedad: dato["novedad"].stringValue, tipo_iva: dato["iva"].stringValue,
                                                  tamanio: dato["tamanio"].stringValue)
                        result.append(article)
                    }
                    completionHandler(result)
                }
            case .failure(let error):
                print(error)
                completionHandler(nil)
            }
            
        }

    
    
    
    }
    
    
    
    
    

    func detallePedido(usuario:String, numPedidoWeb:String, completionHandler: @escaping ([OrderDetail]?) -> Void){
        var arrayParametros : Parameters
        let url = URL(string:  urlBase + "/detallepedido/")!
        
        arrayParametros = ["usuario":usuario, "numPedidoWeb": numPedidoWeb ]
        
        Alamofire.request(url, method: .get, parameters: arrayParametros, encoding: URLEncoding.default, headers: nil).validate().responseJSON() { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    var result = [OrderDetail]()
                    let control = json["codigoCtr"].stringValue
                    let datos = json["DetallePedido"].arrayValue
                    if control  == "0" {
                        completionHandler(nil)
                    }
                    for dato in datos {
                        let detalle = OrderDetail(
                            des_articulo: dato["des_articulo"].stringValue + " " + dato["tamaño"].stringValue ,
                            cod_articulo: dato["cod_articulo"].stringValue,
                            ref_cliente: dato["ref_cliente"].stringValue,
                            
                            pack: dato["pack"].stringValue,
                            precio_pack: dato["precio_pack"].stringValue,
                            peso: dato["peso"].stringValue,
                            cantidad: dato["cantidad"].stringValue,
                            num_linea: dato["num_linea"].stringValue,
                            novedad: dato["novedad"].stringValue,
                            tipo_iva: dato["tipo_iva"].stringValue)
                        result.append(detalle)
                    }
                    completionHandler(result)
                }
            case .failure(let error):
                print(error)
                completionHandler(nil)
            }
            
        }
        
    }



    
    
    func listaPedidos(usuario:String, completionHandler: @escaping ([OrderHead]?) -> Void){
        var arrayParametros : Parameters
        let url = URL(string:  urlBase + "/listapedidos/")!
//        arrayParametros = ["usuario":usuario ]
        
        arrayParametros = ["usuario":usuario,"nregistros":"0"  ]
        
        Alamofire.request(url, method: .get, parameters: arrayParametros, encoding: URLEncoding.default, headers: nil).validate().responseJSON() { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    var result = [OrderHead]()
                    let datos = json["ListaPedido"].arrayValue
                    for dato in datos {
                        let pedido = OrderHead(fecha: dato["Fecha"].stringValue,
                                            numPedido: dato["NumPedido"].stringValue,
                                            estado: dato["Estado"].stringValue,
                                            numPedidoWeb: dato["NumPedidoWeb"].stringValue)
                        result.append(pedido)
                    }
                    completionHandler(result)
                }
            case .failure(let error):
                print(error)
                completionHandler(nil)
            }
            
        }
        
    }
    



   
    func validarLogin(usuario: String, pass: String, completionHandler: @escaping (UsuarioParametros?) -> Void){
        let url = URL(string:  urlBase + "/Login/")!
        var arrayParametros : Parameters
        arrayParametros = ["Usuario":usuario  , "Pass": pass]
        Alamofire.request(url, method: .get, parameters: arrayParametros, encoding: URLEncoding.default, headers: nil).validate().responseJSON() { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    // si control  == 2
                    if  json["codigoCtr"].stringValue != "1"{
                        let usuarioCtr = UsuarioParametros.init(ver_precios: "1", desc_usuario: "", permite_scan: "1", codigoCtr:json["codigoCtr"].stringValue )
                        completionHandler(usuarioCtr)
                    }else{
                        let usuarioPrarametros = UsuarioParametros.init(ver_precios: json["ver_precios_sn"].stringValue, desc_usuario: json["desc_usuario"].stringValue, permite_scan: json["permite_scan"].stringValue, codigoCtr: json["codigoCtr"].stringValue)
                        completionHandler(usuarioPrarametros)
                    }
                }
            case .failure(let error):
                print(error)
                completionHandler(nil)
            }
        }
    }

    func enviarEmail(usuario: String,  completionHandler: @escaping (_ String:String?) -> Void){
        let url = URL(string:  urlBase + "/enviopass/")!
        var arrayParametros : Parameters
        arrayParametros = ["Usuario":usuario ]
        Alamofire.request(url, method: .get, parameters: arrayParametros, encoding: URLEncoding.default, headers: nil).validate().responseJSON() { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    let result = json["codigoCtr"].stringValue
                    completionHandler(result)
                }
            case .failure(let error):
                print(error)
                completionHandler(nil)
            }
        }
    }

    
    func listaFamilias(usuario: String,  completionHandler: @escaping ([Familia]?) -> Void){
        let url = URL(string:  urlBase + "/buscarfamilia/")!
        var arrayParametros : Parameters
        arrayParametros = ["Usuario":usuario  ]
        Alamofire.request(url, method: .get, parameters: arrayParametros, encoding: URLEncoding.default, headers: nil).validate().responseJSON() { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
//                    let result = json["codigoCtr"].stringValue
                    var result = [Familia]()
                    let datos = json["Familias"].arrayValue
                    for dato in datos {
                        let familia = Familia.init(cod_fam_sub: dato["cod_fam_sub"].stringValue,
                                                   desc_fam_sub: dato["desc_fam_sub"].stringValue)
                        result.append(familia)

                    }
                    completionHandler(result)
                }
            case .failure(let error):
                print(error)
                completionHandler(nil)
            }
        }
    }


}
