//
//  ArticuloSearchViewController.swift
//  DemoPedido
//
//  Created by Alejandro Arce on 4/10/16.
//  Copyright © 2016 ArceGuerra, Alejandro. All rights reserved.
//

import UIKit

class ArticuloSearchViewController: UIViewController,UITextFieldDelegate, UIPopoverPresentationControllerDelegate,UnitEnterViewControllerDelegate,FamiliaTableViewControllerDelegate{
    @IBOutlet weak var codigo: UITextField!
    @IBOutlet weak var ref: UITextField!
    @IBOutlet weak var fam: UITextField!
    @IBOutlet weak var sfam: UITextField!
//    @IBOutlet weak var buscando: UILabel!
    @IBOutlet weak var concepto: UITextField!
    @IBOutlet weak var novedades: UISwitch!

    var viewActiveAllowed : Bool!
    var currentArticulo : Article!
    var familiaSelected : Familia!
    var novedad : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        buscando.isHidden = true
        codigo.delegate = self
        ref.delegate = self
        fam.delegate = self
        concepto.delegate = self
        // Do any additional setup after loading the view.
        

        
        
//        self.navigationController?.navigationBar.backItem?.title = "Anything Else"

    }
    
    override func viewDidAppear(_ animated: Bool) {
        navigationController?.toolbar.isHidden = true
        self.title = "Buscar"
        ponerEnBlanco(all: true)


   
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: familiaAction action
    
    @IBAction func familiaAction(_ sender: Any) {
        let vc = (self.storyboard?.instantiateViewController(withIdentifier: "ListaFamilias"))! as! FamiliaTableViewController
        //        vc.delegate = self
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)

        
        
        
    }
    
    
        // MARK: delegate textField
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
//            buscando.isHidden = true
            ponerEnBlanco(all: true)
        
        

        

    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if (textField.tag == 1){
            codigo.resignFirstResponder()
        }
        if (textField.tag == 2){
            ref.resignFirstResponder()
        }
        if (textField.tag == 3){
            concepto.resignFirstResponder()
        }
        if (textField.tag == 4){
            sfam.resignFirstResponder()
        }
        
        if textField.text == ""{ return true}
        
        
//        buscando.isHidden = false
        
        buscarOKAction(self)
        
        
        return true
    }
    
    
    
    
    // MARK: actions
    
    @IBAction func novedadesAction(_ sender: Any) {
        if novedades.isOn {
            ponerEnBlanco(all: false)
            novedad = "1"
            buscarOKAction(self)
        }
        
        
    }
    @IBAction func cancelAction(_ sender: AnyObject) {
       
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buscarOKAction(_ sender: AnyObject) {
        
        
//        self.pedirArticulos()
        DispatchQueue.main.async {
            self.pedirArticulos()

//        let vc = (self.storyboard?.instantiateViewController(withIdentifier: "listaArticulos"))! as! ListaArticulosTableViewController
////        vc.delegate = self
//        self.navigationController?.pushViewController(vc, animated: true)

        }
        
    }
    
      // MARK: delegate unitEnter
    func cancelUnitEnter() {
    }
    func unitEnterWaschanged() {
    }
    
    
      // MARK:   pedir articulos
    
    func pedirArticulos() {
        
        if (existeConexionInternet()){
            
            cargarArticulos()
            
        }else {
            
            alerta.mostrarAlertaSinInternet(targetVC: self)
            ponerEnBlanco(all: true)
            
        }
        
    }
    
    func cargarArticulos(){
        
        
        alerta.iniciarFastLoading(targetVC: self, mensaje: "")
        
        
      
        dataProvider.buscarArticulo(usuario: currentUser!, cod_articulo: codigo.text!, ref_art_cliente: ref.text!, cod_familia: familiaSelected!.cod_fam_sub, codigo_EAN_DUN: "", des_articulo: concepto.text!, novedad: novedad) { articulos in
            
            alerta.pararLoading(targetVC: self)
            if let articulos = articulos {
                if articulos.count < 1 {
                  // sin resultados
                    let alertController = UIAlertController(title: "Sin resultados", message: "Pruebe con otra selección ", preferredStyle: .actionSheet)
                    alertController.view.tintColor = UIColor(red: 1.0, green: 0.50, blue: 0.0, alpha: 1.0)
                    
                    let oKAction = UIAlertAction(title: "OK", style: .default, handler: {
                        alert -> Void in
                        self.ponerEnBlanco(all: true)
                  
                    })
                    alertController.addAction(oKAction)
                    self.present(alertController, animated: true, completion: nil)
//                    alerta.mostrarAlerta(targetVC: self, titulo: "Sin resultados", mensaje:"Pruebe con otra selección ")
//                   return
                }else{
                    // artículos encontrados en la lista
                    articles = articulos
                    let vc = (self.storyboard?.instantiateViewController(withIdentifier: "listaArticulos"))! as! ListaArticulosTableViewController
                    //        vc.delegate = self
                    self.navigationController?.pushViewController(vc, animated: true)

                }
            }else{
                // sin servicio
                alerta.mostrarAlertaSinServicio(targetVC: self)
            }
            
        }
        
    }
    
    // MARK: FamiliaTableViewControllerDelegate
    func familiaWasSelected(familia: Familia) {
     
        ponerEnBlanco(all: false)
        familiaSelected = familia
        fam.text = familia.desc_fam_sub
        print(familia.desc_fam_sub)
        buscarOKAction(self)
        
        
    }

    
    // MARK: functions
    
    func ponerEnBlanco(all: Bool){
        codigo.text = ""
        ref.text = ""
        fam.text = ""
//        sfam.text = ""
        concepto.text = ""
        familiaSelected = Familia.init(cod_fam_sub: "",
                                    desc_fam_sub: "")
        
        if all{
            novedades.setOn(false, animated: true)
            novedad = "0"
        }
    }
    
    
    


}
