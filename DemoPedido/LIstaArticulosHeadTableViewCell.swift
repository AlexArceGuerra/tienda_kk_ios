//
//  LIstaArticulosHeadTableViewCell.swift
//  DemoPedido
//
//  Created by Alejandro Arce on 7/8/17.
//  Copyright © 2017 ArceGuerra, Alejandro. All rights reserved.
//

import UIKit

class LIstaArticulosHeadTableViewCell: UITableViewCell {

    @IBOutlet weak var precio: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
  
    func updateHead(){
        if currentUserParametros.ver_precios{
            precio.isHidden = false
        }else{
            
            precio.isHidden = true
        }

        
       
    }

}
