//
//  NuevoPedidoTableViewController.swift
//  DemoPedido
//
//  Created by ArceGuerra, Alejandro on 19/10/16.
//  Copyright © 2016 ArceGuerra, Alejandro. All rights reserved.
//

import UIKit

class NuevoPedidoTableViewController: UITableViewController, UnitEnterViewControllerDelegate,NuevoPedidoTableViewCelldelegate,ScannerViewControllerDelegate{
  

    @IBOutlet weak var deletButton: UIBarButtonItem!
    @IBOutlet weak var scanButton: UIBarButtonItem!
    @IBOutlet weak var searchButton: UIBarButtonItem!
    @IBOutlet weak var lockButton: UIBarButtonItem!
    @IBOutlet weak var numLineas: UIBarButtonItem!
    var currentOrderLocal: OrderHead!
    
    
    
    var  currentIndexRow: Int!
    var editable : Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
      // print(currentOrder)
       // pedirDetallePedido()
        

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        navigationController?.toolbar.isHidden = false
        
        pedirDetallePedido()

        
        
        
        if currentUserParametros.permite_scan{
             scanButton.isEnabled = editable
        }else{
            scanButton.title = ""
            scanButton.isEnabled = false
    
        }
        
        deletButton.isEnabled = editable
        searchButton.isEnabled = editable
        lockButton.isEnabled = editable
        

    }
    
    override func viewDidAppear(_ animated: Bool) {

        tableView.reloadData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        numLineas.title = String(orderDetails.count) + " lineas"
        return orderDetails.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! NuevoPedidoTableViewCell

        cell.delegate = self
        cell.updateCell(indexRowOrderDetailFrom: indexPath.row,editable: editable)
        
        return cell
        

    }
    
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:  "HeadCell") as! NuevoPedidoHeadTableViewCell
        cell.updateHead()

        return cell
        
    }
    override func  tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 25
        
    }
    
    override func  tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier:  "FootCell") as! NuevoPedidoFooterTableViewCell
        
        cell.updateFooter(orderDets: orderDetails)
//        var sun : Float = 0
//        for ordDet : OrderDetail in orderDetails {
//            sun += Float(ordDet.cant) * ordDet.art.pricePack
//            
//        }
//        
//        cell.sumImporte.text = String(format: "%.1f", sun)
        return cell

    }
    
    
    
    
    override func  tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
           return 25
        
        
    }
    
    
    
    

    
 //MARK:actions
    
    @IBAction func lockAction(_ sender: Any) {
        
        
        let alertController = UIAlertController(title: "Cerrar pedido.", message: "Esta seguro ?", preferredStyle: UIAlertControllerStyle.actionSheet)
        alertController.view.tintColor = UIColor(red: 1.0, green: 0.50, blue: 0.0, alpha: 1.0)
        
        let siAction = UIAlertAction(title: "Cerrar", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            //delete pedido
            alerta.iniciarFastLoading(targetVC: self, mensaje: "")
            dataProvider.lockPedido(usuario: currentUser, numpedidoweb: currentOrder.numPedidoWeb) { contr in
                alerta.pararLoading(targetVC: self)
                if let contr = contr {
                    if contr == "NODEL"{
                        alerta.mostrarAlerta(targetVC: self, titulo: " Error ", mensaje: "Imposible borrar este pedido ")
                    }else{
                        let alertControllerTwo = UIAlertController(title: String(format: "El pedido: %@",currentOrder.numPedido), message: "Ha sido transmitido a Conway", preferredStyle: UIAlertControllerStyle.actionSheet)

                        let siActionnn = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                            self.navigationController?.popViewController(animated: true)
                        
                        }
                        
                        alertControllerTwo.addAction(siActionnn)
                        self.present(alertControllerTwo, animated: true, completion: nil)

                    }
                    
                }else{
                    alerta.mostrarAlertaSinServicio(targetVC: self)
                }}
            
        }
        let noAction = UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            return
        }
        
        alertController.addAction(siAction)
        alertController.addAction(noAction)
        
        self.present(alertController, animated: true, completion: nil)
        
        
        
    }
    @IBAction func clonarAction(_ sender: Any) {
        
        let alertController = UIAlertController(title: "Duplicar pedido", message: "", preferredStyle: .alert)
        alertController.view.tintColor = UIColor(red: 1.0, green: 0.50, blue: 0.0, alpha: 1.0)

        
        let saveAction = UIAlertAction(title: "OK", style: .default, handler: {
            alert -> Void in
            
            let varnumpedido = alertController.textFields![0] as UITextField
            
            alerta.iniciarFastLoading(targetVC: self, mensaje: "")
            
            
            //nuevo pedido
            dataProvider.clonarPedido(usuario: currentUser, numpedido: varnumpedido.text!, numpedidoweb: currentOrder.numPedidoWeb) { numpedidoweb in
                alerta.pararLoading(targetVC: self)
                if let numpedidoweb = numpedidoweb {
                    if numpedidoweb == "NOADD"{
                        alerta.mostrarAlerta(targetVC: self, titulo: varnumpedido.text!, mensaje: " ! Este pedido ya existe !")
                    }else{
                        print (numpedidoweb)
                        
//                        self .pedirListaPedidos()
                        
                        
                                         self.navigationController?.popViewController(animated: true)
                        
                        //                    currentOrder = numpedido
                        //                    self.showDetallePedio()
                        
                    }
                    
                    
                }else{
                    alerta.mostrarAlertaSinServicio(targetVC: self)
                }
            }
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Nombre o número "
        }
        //        alertController.addTextField { (textField : UITextField!) -> Void in
        //            textField.placeholder = "Enter Second Name"
        //        }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
        
        
    }
    @IBAction func deleteOrderAction(_ sender: Any) {

        
        let alertController = UIAlertController(title: "Borrar pedido.", message: "Esta seguro ?", preferredStyle: UIAlertControllerStyle.actionSheet)
        alertController.view.tintColor = UIColor(red: 1.0, green: 0.50, blue: 0.0, alpha: 1.0)
        
        let siAction = UIAlertAction(title: "Borrar", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            //delete pedido
            alerta.iniciarFastLoading(targetVC: self, mensaje: "")
            dataProvider.deletePedido(usuario: currentUser, numpedidoweb: currentOrder.numPedidoWeb) { contr in
                alerta.pararLoading(targetVC: self)
                if let contr = contr {
                    if contr == "NODEL"{
                        alerta.mostrarAlerta(targetVC: self, titulo: " Error ", mensaje: "Imposible borrar este pedido ")
                    }else{
                        self.navigationController?.popViewController(animated: true)

                    }
                    
                }else{
                    alerta.mostrarAlertaSinServicio(targetVC: self)
                }}
            
        }
        let noAction = UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            return
        }

        alertController.addAction(siAction)
        alertController.addAction(noAction)

        self.present(alertController, animated: true, completion: nil)

       
        
        
        
        
        
        
        
//        //delete pedido
//        alerta.iniciarFastLoading(targetVC: self, mensaje: "")
//        dataProvider.deletePedido(usuario: currentUser, numpedidoweb: currentOrder.numPedidoWeb) { contr in
//            alerta.pararLoading(targetVC: self)
//            if let contr = contr {
//                if contr == "NODEL"{
//                    alerta.mostrarAlerta(targetVC: self, titulo: " Error ", mensaje: "Imposible borrar este pedido ")
//                }
//                
//            }else{
//                alerta.mostrarAlertaSinServicio(targetVC: self)
//            }}
        
        
    }
    
    @IBAction func backAction(_ sender: AnyObject) {
        
//        var navig = UINavigationController()
        
        
        navigationController?.popViewController(animated: true)
//        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func searchAction(_ sender: AnyObject) {
        
        let vc = (storyboard?.instantiateViewController(withIdentifier: "searchArti"))! as! ArticuloSearchViewController
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func scanAction(_ sender: AnyObject) {
        let popoverContent = (self.storyboard?.instantiateViewController(withIdentifier: "scanQR"))! as! ScannerViewController
        
       // popoverContent.delegate = self
        
        navigationController?.pushViewController(popoverContent, animated: true)

        
        
//        popoverContent.providesPresentationContextTransitionStyle = true
//        popoverContent.definesPresentationContext = true
//        popoverContent.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//        self.present(popoverContent, animated: true, completion: nil)
    }

    
    //MARK: delegates cell
    func unitChangeTappedOnNuevoPedidoTableViewCell(indexRowOrderDetailFrom: Int){
        showUnitEnter(indexRowOrderDetailFrom: indexRowOrderDetailFrom)
        
    }

    
    // MARK: delegate scanqr
    func scanQRFound(text: String) {
        
        tableView.reloadData()
        
//        let alertController = UIAlertController(title: "Scan QR", message:
//                        text, preferredStyle: UIAlertControllerStyle.alert)
//        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default,handler: nil))
//        
//        self.present(alertController, animated: true, completion: nil)
    }
    
    
    // MARK: delegate UnitEnter
    func unitEnterWaschanged() {
        self.tableView.reloadData()


    }
    func cancelUnitEnter() {
        self.tableView.reloadData()
    }
    
    //MARK: functions
    
    func showUnitEnter(indexRowOrderDetailFrom: Int){
        
        currentIndexRow = indexRowOrderDetailFrom
        currentOrderDetail = orderDetails[currentIndexRow]
        let currArticle = Article.init(des_articulo: currentOrderDetail.des_articulo, cod_articulo: currentOrderDetail.cod_articulo, pack: String(currentOrderDetail.pack), precio_pack: String(currentOrderDetail.precio_pack), peso: String(currentOrderDetail.peso), novedad: currentOrderDetail.novedad, tipo_iva:  String(currentOrderDetail.tipo_iva),tamanio: "")
//        let cant = currentOrderDetail.cantidad
        let vc = (self.storyboard?.instantiateViewController(withIdentifier: "UnitEnter"))! as! UnitEnterViewController
        vc.delegate = self
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        
        present(vc, animated: true, completion: nil)
    
        vc.updateOrderDetailFromArticle(article: currArticle, deleteAllowed: true)
    }
    
    func clearView()  {
        if let viewWithTag = self.view.viewWithTag(101) {
            viewWithTag.removeFromSuperview()
        }

    }
    
    func pedirDetallePedido() {
        if (existeConexionInternet()){
            cargarDetallePedido()
        }else {
            alerta.mostrarAlertaSinInternet(targetVC: self)
        }
        
    }
    
    func cargarDetallePedido(){
        alerta.iniciarFastLoading(targetVC: self, mensaje: "")
        
        currentOrder = currentOrderLocal

        dataProvider.detallePedido(usuario: currentUser, numPedidoWeb: currentOrderLocal.numPedidoWeb) { detalles in
            alerta.pararLoading(targetVC: self)
            if let detalles = detalles {
                DispatchQueue.main.async {
//                    print( currentOrder)
//                    print ( self.currentOrderLocal)
                    orderDetails = detalles
                    self.tableView .reloadData()
                    self.title = self.currentOrderLocal.numPedido
                    currentOrder = self.currentOrderLocal

                }
                
                }else{
                    alerta.mostrarAlertaSinServicio(targetVC: self)
                }
            }
    }
    
    
}
