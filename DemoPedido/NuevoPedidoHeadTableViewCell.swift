//
//  NuevoPedidoHeadTableViewCell.swift
//  DemoPedido
//
//  Created by Mac-27 on 5/7/17.
//  Copyright © 2017 ArceGuerra, Alejandro. All rights reserved.
//

import UIKit

class NuevoPedidoHeadTableViewCell: UITableViewCell {
    @IBOutlet weak var importeLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func updateHead(){
        
        importeLabel.isHidden = !currentUserParametros.ver_precios
        
        
    }

}
