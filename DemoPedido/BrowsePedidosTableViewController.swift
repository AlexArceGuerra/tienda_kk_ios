//
//  BrowsePedidosTableViewController.swift
//  DemoPedido
//
//  Created by ArceGuerra, Alejandro on 25/10/16.
//  Copyright © 2016 ArceGuerra, Alejandro. All rights reserved.
//

import UIKit

class BrowsePedidosTableViewController: UITableViewController {

    var numPedidoWebAdd : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()


        
    }

    override func viewWillAppear(_ animated: Bool) {
    
       navigationController?.toolbar.isHidden = false
        navigationController?.navigationBar.isHidden = false
        
        let backButt = UIBarButtonItem(title: "< Salir", style: .plain, target: self, action: #selector(BrowsePedidosTableViewController.backButtonAction))

        self.navigationItem.leftBarButtonItem = backButt
        



    }
    
    override func viewDidAppear(_ animated: Bool) {

        self .pedirListaPedidos()
        numPedidoWebAdd = nil
       
        
        self.title = "Pedidos"

        // mejoras Ptes
       // self.title = currentUserParametros.desc_usuario
        


    }
    
    
    
    func backButtonAction() {
     
        navigationController?.popViewController(animated: true)

        
        
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return ordersHead.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! BrowsePedidosTableViewCell

//        cell.delegate = self
//        cell.currentOrderHead = ordersHead[indexPath.row]
        currentOrder = ordersHead[indexPath.row]
        cell .updatecell()
        
        

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        currentOrder = ordersHead[indexPath.row]

        let vc = (self.storyboard?.instantiateViewController(withIdentifier: "pedidoNew"))! as! NuevoPedidoTableViewController
        vc.editable = currentOrder.estado
        vc.currentOrderLocal = currentOrder

        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
   
    
    
//    -(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//    {
//    if([indexPath row] == lastRow){
//    //end of loading
//    //for example [activityIndicator stopAnimating];
//    }
//    }


    
    func showDetallePedio() {
        let vc = (self.storyboard?.instantiateViewController(withIdentifier: "pedidoNew"))! as! NuevoPedidoTableViewController
        vc.editable = currentOrder.estado
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    

    @IBAction func testAction(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }


    @IBAction func addAction(_ sender: Any) {
//        alerta.mostrarAlertaAction(targetVC: self, titulo:  "Nuevo pedido", mensaje: "")
        
        let alertController = UIAlertController(title: "Nuevo pedido", message: "", preferredStyle: .alert)
        
        alertController.view.tintColor = UIColor(red: 1.0, green: 0.50, blue: 0.0, alpha: 1.0)

        numPedidoWebAdd = nil

        let saveAction = UIAlertAction(title: "OK", style: .default, handler: {
            alert -> Void in
            
            let varnumpedido = alertController.textFields![0] as UITextField
            
            alerta.iniciarFastLoading(targetVC: self, mensaje: "")
            
            
            //nuevo pedido
              dataProvider.nuevoPedido(usuario: currentUser, numpedido: varnumpedido.text!) { numpedidoweb in
                alerta.pararLoading(targetVC: self)
                if let numpedidoweb = numpedidoweb {
                   if numpedidoweb == "NOADD"{
                        alerta.mostrarAlerta(targetVC: self, titulo: varnumpedido.text!, mensaje: " ! Este pedido ya existe !")
                   }else{
                    print (numpedidoweb)
                    self.numPedidoWebAdd = numpedidoweb
                    self .pedirListaPedidos()

                    }
                    
                    
                }else{
                    alerta.mostrarAlertaSinServicio(targetVC: self)
                }
            }

        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Nombre o número "
        }
//        alertController.addTextField { (textField : UITextField!) -> Void in
//            textField.placeholder = "Enter Second Name"
//        }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
        
        
        
    }
    
    
    
    func goToOrder(){
        
        for curr in ordersHead{
            if curr.numPedidoWeb == numPedidoWebAdd{
              currentOrder = curr
                
                let vc = (self.storyboard?.instantiateViewController(withIdentifier: "pedidoNew"))! as! NuevoPedidoTableViewController
                vc.editable = currentOrder.estado
                vc.currentOrderLocal = currentOrder
                self.navigationController?.pushViewController(vc, animated: true)

                
                
            }
            
            
        }
        
        
        
    }

    func pedirListaPedidos() {
        let alert = Alerta()

        if (existeConexionInternet()){
            
            cargarDatosPedidos()
            
        }else {
            
            alert.mostrarAlertaSinInternet(targetVC: self)
            
        }
        
    }
    
    func cargarDatosPedidos(){
        var  showWaiting = true
        let alert = Alerta()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            if showWaiting{
              alert.iniciarFastLoadingTag(targetVC: self, mensaje: "", tag: 105)
            }
        })

        
        
        //pedidos
        dataProvider.listaPedidos(usuario: currentUser) { pedidos in
                alert.pararLoadingTag(targetVC: self, tag: 105)
                showWaiting = false
            if let pedidos = pedidos {
                DispatchQueue.main.async {
                    print( pedidos.count)
                    ordersHead = pedidos
                    self.tableView .reloadData()
                    if (self.numPedidoWebAdd) != nil{
                        self.goToOrder()

                        
                    }
                    
                    
                    
                }
            }else{
                alert.mostrarAlertaSinServicio(targetVC: self)
            }
        }
    }
    
    

}
