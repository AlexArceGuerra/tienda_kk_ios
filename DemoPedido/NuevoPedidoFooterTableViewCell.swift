//
//  NuevoPedidoFooterTableViewCell.swift
//  DemoPedido
//
//  Created by Alejandro Arce on 3/11/16.
//  Copyright © 2016 ArceGuerra, Alejandro. All rights reserved.
//

import UIKit

class NuevoPedidoFooterTableViewCell: UITableViewCell {
    @IBOutlet weak var sumImporte: UILabel!
    @IBOutlet weak var sumPeso: UILabel!
    @IBOutlet weak var ivaEurosLabel: UILabel!
    @IBOutlet weak var eurosLabel: UILabel!

    @IBOutlet weak var sumIva: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateFooter(orderDets: [OrderDetail]){
        var sun : Float = 0
        var peso : Float = 0
        var iva : Float = 0
        for ordDet : OrderDetail in orderDets {
            sun += Float(ordDet.cantidad) * Float(ordDet.precio_pack)
            peso += Float(ordDet.cantidad) * Float(ordDet.peso)
            iva += Float(ordDet.cantidad) * Float(ordDet.precio_pack) * (1 + Float(ordDet.tipo_iva)/100)
            
        }
        sumImporte.text = String(format: "%.1f", sun)
        sumPeso.text = String(format: "%.1f", peso)
        sumIva.text = String(format: "%.1f", iva)
        
        if !currentUserParametros.ver_precios{
            ivaEurosLabel.isHidden = true
            eurosLabel.isHidden = true
            sumImporte.isHidden = true
            sumIva.isHidden = true
        }


    }

}
