//
//  ListaArticulosTableViewController.swift
//  DemoPedido
//
//  Created by Alejandro Arce on 5/5/17.
//  Copyright © 2017 ArceGuerra, Alejandro. All rights reserved.
//

import UIKit


class ListaArticulosTableViewController: UITableViewController,UnitEnterViewControllerDelegate {

//    var delegate : ListaArticulosTableViewControllerDelegate?
    var articleSeleted : Article!
//    var isAddLine : Bool!
    var indexSelected : Int!
    var articleAction : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        tableView.reloadData()
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return articles.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ArtCell", for: indexPath) as! ListaArticulosTableViewCell
        cell.updateCell(indexRow: indexPath.row)
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
       
        
        let cell = tableView.dequeueReusableCell(withIdentifier:  "HeadArtCell") as! LIstaArticulosHeadTableViewCell
        
        cell.updateHead()
        return cell
        
    }

    override func  tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 35
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return 35
    }

    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var selectedItem = indexPath
        articleSeleted = articles[selectedItem.row]
       
        
        
        let vc = (self.storyboard?.instantiateViewController(withIdentifier: "UnitEnter"))! as! UnitEnterViewController
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
       
        vc.updateOrderDetailFromArticle(article: articleSeleted, deleteAllowed: false)

    }

    
    
    
    
    
    
    
  // MARK: - delegate UnitEnterViewControllerDelegate
    func unitEnterWaschanged() {
//        navigationController?.popViewController(animated: true)

}
    
    
    
}
