//
//  NuevoPedidoTableViewCell.swift
//  DemoPedido
//
//  Created by ArceGuerra, Alejandro on 19/10/16.
//  Copyright © 2016 ArceGuerra, Alejandro. All rights reserved.
//

import UIKit

protocol NuevoPedidoTableViewCelldelegate {
    func unitChangeTappedOnNuevoPedidoTableViewCell(indexRowOrderDetailFrom: Int)
    
}


class NuevoPedidoTableViewCell: UITableViewCell {

    @IBOutlet var des: UILabel!
    @IBOutlet var pack: UILabel!
    @IBOutlet var precio: UILabel!
    @IBOutlet var unidades: UILabel!
    @IBOutlet var importe: UILabel!
    @IBOutlet weak var uniChange: UIButton!
    @IBOutlet weak var referencia: UILabel!
 
    @IBOutlet weak var codigo: UILabel!
    @IBOutlet weak var novedad: UILabel!
    var currentOrderDetail : OrderDetail!
    var currentIndexOrderDetailFrom: Int!
    
    var delegate : NuevoPedidoTableViewCelldelegate?
    
    
    
    
    override func awakeFromNib() {
      
        super.awakeFromNib()
        // Initialization code
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func updateCell(indexRowOrderDetailFrom: Int, editable: Bool){
        
        let orderDetail = orderDetails[indexRowOrderDetailFrom]
        currentOrderDetail = orderDetail
        currentIndexOrderDetailFrom = indexRowOrderDetailFrom
//        let defColor =  des.backgroundColor
        
        des.text = orderDetail.des_articulo
        pack.text =  String(format: "Pack: %d", orderDetail.pack)
        precio.text = String(format: "%.1f €", orderDetail.precio_pack)
        unidades.text = String(orderDetail.cantidad)
        // mejoras Ptes
        referencia.text = ""
        if orderDetail.ref_cliente != ""{
            referencia.text = String(format: "Ref: %@", orderDetail.ref_cliente)
        }
        if orderDetail.cod_articulo != ""{
            codigo.text = String(format: "Cod: %@", orderDetail.cod_articulo)
        }

        
//        unidades.text = String(format: "%.d", orderDetail.cantidad)
        let importeCal = Float(orderDetail.precio_pack) * Float(orderDetail.cantidad)
        importe.text = String(format: "%.1f", importeCal)
        uniChange.isHidden = !editable
        if editable {
        unidades.textColor = unidades.tintColor
        }
        
        if orderDetail.novedad == "S"{
            novedad.isHidden = false
        }else{
            novedad.isHidden = true

        }
        
        precio.isHidden = true
        importe.isHidden = true
        if currentUserParametros.ver_precios{
            precio.isHidden = false
            importe.isHidden = false
        }
        
        
        
    }
    
    
    @IBAction func unitChangeAction(_ sender: AnyObject) {
        delegate?.unitChangeTappedOnNuevoPedidoTableViewCell(indexRowOrderDetailFrom: currentIndexOrderDetailFrom)
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
}
