//
//  ListaArticulosTableViewCell.swift
//  DemoPedido
//
//  Created by Alejandro Arce on 5/5/17.
//  Copyright © 2017 ArceGuerra, Alejandro. All rights reserved.
//

import UIKit

class ListaArticulosTableViewCell: UITableViewCell {

    

    @IBOutlet weak var descripcion: UILabel!
    @IBOutlet weak var precio: UILabel!
    
    @IBOutlet weak var tamanio: UILabel!
    
    @IBOutlet weak var pack: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func updateCell(indexRow: Int){

        let currArti  = articles[indexRow]
        descripcion.text = currArti.des_articulo
        tamanio.text = currArti.tamanio
        pack.text = String(format: "%6d", currArti.pack)
        
        //peso.text =  String(format: "%.1f", currArti.peso)
        precio.text = String(format: "%.1f €", currArti.precio_pack)

        if currentUserParametros.ver_precios{
            precio.text = String(format: "%.1f €", currArti.precio_pack)
        }else{
            
            precio.text = ""
        }
        
        
    
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
