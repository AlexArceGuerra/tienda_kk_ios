//
//  UnitEnterViewController.swift
//  DemoPedido
//
//  Created by Alejandro Arce on 23/10/16.
//  Copyright © 2016 ArceGuerra, Alejandro. All rights reserved.
//


import UIKit
protocol UnitEnterViewControllerDelegate {
    func unitEnterWaschanged()
}



class UnitEnterViewController: UIViewController, UITextFieldDelegate {
   
    var delegate: UnitEnterViewControllerDelegate?

    @IBOutlet weak var numberTE: UITextField!
    @IBOutlet var descrip: UILabel!
    @IBOutlet var price: UILabel!
    @IBOutlet var pack: UILabel!
    @IBOutlet weak var codigo: UILabel!
//    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var okButton: UIBarButtonItem!
    @IBOutlet weak var deleteButton: UIBarButtonItem!
    
    var articleSelected : Article!
    var indexInOrderDetail : Int!
    var articleAction: String!
   // var alerta: Alerta!
    
    
//    var indexSelected : Int!
//    var articleAction : String!


    override func viewDidLoad() {
        super.viewDidLoad()
        numberTE.delegate = self
        numberTE.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        okButton.isEnabled = true
        deleteButton.isEnabled = true

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
     // MARK:  functions
    
    func iniControles_Variables(article: Article, cant: Int, deleteAllowed: Bool!){
        articleSelected = article

        codigo?.text = String(format: "Código: %@",article.cod_articulo)
        descrip?.text = article.des_articulo
        pack?.text =  String(format: "Pack: %d", article.pack)
        price?.text =  String(format: "Precio: %.1f", article.precio_pack)
       okButton.isEnabled = false
        deleteButton.isEnabled = deleteAllowed

        numberTE.text = ""
        if cant != 0 {
            numberTE.text = String (cant)
        }
        
     
        price?.text = ""
        if currentUserParametros.ver_precios{
            price?.text =  String(format: "Precio: %.1f", article.precio_pack)

        }
        
        
        numberTE.becomeFirstResponder()

    }
    
    
//    func updateOrderDetailFromArticle(article: Article, cant: Int, indexInOrderDetailFrom: Int, action: String, deleteAllowed: Bool!)  {

    func updateOrderDetailFromArticle(article: Article, deleteAllowed: Bool!)  {
       
        let cantt = searchCantInOrderDetails(selectedArticle: article)
        iniControles_Variables(article: article,cant: cantt, deleteAllowed: deleteAllowed)


    }
    
    
    

     // MARK:  actions
    @IBAction func deleteAction(_ sender: Any) {
        
        let alertController = UIAlertController(title: "Borrar del pedido", message: "Estas seguro ?", preferredStyle: UIAlertControllerStyle.actionSheet)
        alertController.view.tintColor = UIColor(red: 1.0, green: 0.25, blue: 1.0, alpha: 1.0)
        let okAction = UIAlertAction(title: "Si", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            self.updateRemoteArticle(action: "delete", articleRemoteOrder: self.articleSelected, indexArticleLocalOrderArray: self.indexInOrderDetail, cant: 0)
             self.dismiss(animated: true, completion: nil)
        }
        let cancelAction = UIAlertAction(title: "Cancelar ", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
             self.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
        
    }
    @IBAction func cancelAction(_ sender: AnyObject) {
    
        self.dismiss(animated: true, completion: nil)
    
    }
    
    
    @IBAction func okAction(_ sender: AnyObject) {
        if let cant =  Int(numberTE.text!){
            if cant > 9000000 {
                alerta.mostrarAlerta(targetVC: self, titulo: "Demasiadas unidades.", mensaje: "")
            }else{
    
            updateRemoteArticle(action: articleAction, articleRemoteOrder: articleSelected, indexArticleLocalOrderArray: indexInOrderDetail, cant: cant)
            }
        }

        
    }
    
    // MARK: - functions
    func searchCantInOrderDetails(selectedArticle: Article) -> Int {
        
        articleAction = "insert"
        indexInOrderDetail = 0

        if orderDetails.count < 1 {
            return 0
        }
        for index in 0...orderDetails.count-1{
            let currArticle = orderDetails[index]
            if currArticle.cod_articulo == selectedArticle.cod_articulo {
                articleAction = "update"
                indexInOrderDetail = index
                return currArticle.cantidad
            }
        }
        return 0
    }

    
    
    
     // MARK:  delegate text field
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
         okAction(self)
        return true
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        okButton.isEnabled  = true
    }

    // MARK: updating article
    func updateRemoteArticle(action: String, articleRemoteOrder: Article, indexArticleLocalOrderArray: Int, cant: Int) {
        
        
        
        
        var vNum_lin = "0"
        
        if action == "update" || action == "delete"{
            vNum_lin = String(orderDetails[indexArticleLocalOrderArray].num_linea)
            
        }
        
        alerta.iniciarFastLoading(targetVC: self, mensaje: "")
        dataProvider.upLoadOrderDetail(usuario: currentUser, numpedidoweb: currentOrder.numPedidoWeb, cod_articulo: articleRemoteOrder.cod_articulo, cantidad: String(cant), num_linea: vNum_lin, accion: action){upLoadOrderDetail in
            alerta.pararLoading(targetVC: self)
            if let upLoadOrderDetail = upLoadOrderDetail {
                DispatchQueue.main.async {
                    if upLoadOrderDetail.codigo_ctr == "1"{
                        //add codigo
                        // inArray
                        switch action{
                        case "insert":
                            // INSERT
                            let addOrderLine: OrderDetail = OrderDetail.init(
                                des_articulo: articleRemoteOrder.des_articulo,
                                cod_articulo: articleRemoteOrder.cod_articulo,
                                ref_cliente: "",
                                pack: String(articleRemoteOrder.pack),
                                precio_pack: String(articleRemoteOrder.precio_pack),
                                peso: String(articleRemoteOrder.peso),
                                cantidad: String(cant),
                                num_linea: upLoadOrderDetail.num_lin,
                                novedad: articleRemoteOrder.novedad,
                                tipo_iva: String(articleRemoteOrder.tipo_iva))
                            orderDetails.append(addOrderLine)
                            
                        case "update":
                            // UPDATE
                            orderDetails[indexArticleLocalOrderArray].cantidad = cant
                        case "delete":
                            //DELETE
                            orderDetails.remove(at: indexArticleLocalOrderArray)
                            
                        default:
                            print("")
                        }
                        self.delegate?.unitEnterWaschanged()
                        self.dismiss(animated: true, completion: nil)


                    }else{
                        alerta.mostrarAlertaAction(targetVC: self, titulo: "Sin acceso a este pedido" , mensaje: "Actualize la lista de pedidos y vuelvalo a intentar")

                        let saveAction = UIAlertAction(title: "OK", style: .default, handler: {
                            alert -> Void in
                            self.dismiss(animated: true, completion: nil)
                        })
                        alerta.alert.addAction (saveAction)

                        
                    }
                }
            }else{
                alerta.mostrarAlertaSinServicio(targetVC: self)
            }
        }
        
    }




}
