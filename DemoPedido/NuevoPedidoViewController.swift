//
//  NuevoPedidoViewController.swift
//  DemoPedido
//
//  Created by ArceGuerra, Alejandro on 27/9/16.
//  Copyright © 2016 ArceGuerra, Alejandro. All rights reserved.
//

import UIKit

class NuevoPedidoViewController: UIViewController,UITextFieldDelegate,UITableViewDelegate, UITableViewDataSource{

   
    @IBOutlet var Ok: UIButton!
    @IBOutlet var fecha: UILabel!
    @IBOutlet  weak var numero: UITextField!
    @IBOutlet var detalleTableView: UITableView!
    @IBOutlet var descripcion: UILabel!
    @IBOutlet weak var addButton: UIButton!
   
    let swiftBlogs = ["Ray Wenderlich", "NSHipster", "iOS Developer Tips", "Jameson Quave", "Natasha The Robot", "Coding Explorer", "That Thing In Swift", "Andrew Bancroft", "iAchieved.it", "Airspeed Velocity","Alejandro Arce"]
   
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        Ok.isHidden = true
        fecha.isHidden = true
        numero.delegate = self
        numero.resignFirstResponder()
        detalleTableView.delegate = self
        detalleTableView.dataSource = self
        
        // Do any additional setup after loading the view.
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func OkButton(_ sender: AnyObject) {
        
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy"
        let dateString = dateFormatter.string(from: date)
        
        fecha.text = String(format: "pepe %@", dateString);
        fecha.isHidden = false;
        
        
    }
    // UITextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        Ok.isHidden = false
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("TextField did end editing method called")
    }
    //    @nonobjc func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
    //        print("TextField should begin editing method called")
    //        return true;
    //    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("TextField should clear method called")
        Ok.isHidden = true
        return true;
    }
    
    
    //    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
    //        print("TextField should snd editing method called")
    //        return true;
    //    }
    //    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
    //        print("While entering the characters this method gets called")
    //       Ok.isHidden = false
    //        return true;
    //    }
    //    func textFieldShouldReturn(textField: UITextField) -> Bool {
    //        print("TextField should return method called")
    //        textField.resignFirstResponder();
    //        return true;
    //    }
    
    
    
    
    //  TableView Delagate
    
    //    func numberOfSectionsInTableView(_tableView: UITableView) -> Int {
    //        return 1
    //    }
    //    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    //        return swiftBlogs.count
    //    }
    //
    //    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    //        let cell = tableView.dequeueReusableCell(withIdentifier: textCellIdentifier, for: indexPath as IndexPath)
    //
    //        let row = indexPath.row
    //        cell.textLabel?.text = swiftBlogs[row]
    //
    //        return cell
    //
    //    }
    //
    
    //    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    //        let cell = tableView.dequeueReusableCell(withIdentifier: textCellIdentifier, for: indexPath as IndexPath)
    //
    //        let row = indexPath.row
    //        cell.textLabel?.text = swiftBlogs[row]
    //
    //        return cell
    //    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
         let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
//        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! NuevoPedidoCellTableViewCell

        
        //        cell.textLabel?.text = "Descripción artículo \(indexPath.section) Row \(indexPath.row)  \"
        
       
        
        cell.textLabel?.text =
         String(format: "Artículo %d", indexPath.row).padding(toLength: 25, withPad: " ", startingAt: 0)   +
        "10" +
        "    123"
      
        
        //        cell.detailTextLabel?.text = "123.34"
//        cell.textLabel?.text = "Descripción artículo \(indexPath.row) "
//        var image : UIImage?
//        image = UIImage(named: "Image_1")
//        cell.imageView?.image = image
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier:  "redCell")
        
        
        
        return cell
        
        
    }
    func  tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    
    
    
    //    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
    //
    //    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
