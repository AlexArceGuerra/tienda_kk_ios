//
//  BrowsePedidosTableViewCell.swift
//  DemoPedido
//
//  Created by ArceGuerra, Alejandro on 25/10/16.
//  Copyright © 2016 ArceGuerra, Alejandro. All rights reserved.
//

import UIKit
//protocol BrowsePedidosTableViewCellDelegate {
//    func buttonActionOnOrderHeadCell(orderHead: OrderHead)
//}



class BrowsePedidosTableViewCell: UITableViewCell {

    @IBOutlet var dia: UILabel!
    @IBOutlet var diaSemana: UILabel!
    @IBOutlet var año: UILabel!
    @IBOutlet var mes: UILabel!

    @IBOutlet var unLock: UIImageView!
    @IBOutlet var numeroRef: UILabel!
    
//    var currentOrderHead : OrderHead!
//    var currentPedido : Pedido!
//    var delegate : BrowsePedidosTableViewCellDelegate?
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        // Initialization code
    }
    @IBAction func detailButton(_ sender: AnyObject) {
        
        
    }
    
    func updatecell(){
       
        let pedido = currentOrder!
        
        let dateString = pedido.fecha
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        dateFormatter.locale = Locale.current
        let dateObj = dateFormatter.date(from: dateString)

        dateFormatter.dateFormat = "dd"
        dia.text = dateFormatter.string(from: dateObj!)
        
        dateFormatter.dateFormat = "MMM"
        mes.text = dateFormatter.string(from: dateObj!).capitalized
        
        dateFormatter.dateFormat = "yyyy"
        año.text = dateFormatter.string(from: dateObj!)

        dateFormatter.dateFormat = "EEE"
        diaSemana.text = dateFormatter.string(from: dateObj!)
        
        
        
        numeroRef.text = String (  pedido.numPedido)
        unLock.isHidden = pedido.estado
       
        var estadoColor = UIColor .black
        if unLock.isHidden{
            estadoColor  = UIColor .red
        }
        dia.textColor = estadoColor
        diaSemana.textColor = estadoColor
        año.textColor = estadoColor
        mes.textColor = estadoColor
        numeroRef.textColor = estadoColor

        
      
    }
    
//    @IBAction func actionButton(_ sender: AnyObject) {
//        
//            delegate?.buttonActionOnOrderHeadCell(orderHead: currentOrder)
// 
//    }
        
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
