//
//  LoginViewController.swift
//  DemoPedido
//
//  Created by Alejandro Arce on 3/10/16.
//  Copyright © 2016 ArceGuerra, Alejandro. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController,UITextFieldDelegate{

    @IBOutlet weak var usuario: UITextField!
    @IBOutlet weak var pass: UITextField!
    @IBOutlet weak var entrar: UIButton!
    @IBOutlet weak var versionLabel: UILabel!
    
//    let dataProvider = ConnectionLocalService()
//    let dataProvider = ConnectionRemoteService()
//    let alerta = Alerta()
//    var pedidos : [Pedido] = [Pedido]()


    override func viewDidLoad() {
        super.viewDidLoad()
        entrar.isHidden = false
        pass.delegate = self
        
        
        


        // Do any additional setup after loading the view.
    }

    
    override func viewWillAppear(_ animated: Bool) {
        
        navigationController?.toolbar.isHidden = true
        navigationController?.navigationBar.isHidden = true
        usuario.delegate = self
        pass.delegate = self
        
        usuario.text = (UserDefaults.standard.object(forKey: "Usuario") as? String)!
        pass.text = ""
        
//        pass.text = "3X4M8J0K"
//        usuario.text = "072967"
//        
//        usuario.text = "344440"
//        pass.text = "wedzas"
        
        // version
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        versionLabel.text = "V." + version

 
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func enviarMailAction(_ sender: Any) {
//        dataProvider.enviarEmail(usuario: <#T##String#>, completionHandler: <#T##(String?) -> Void#>)
        
        if (!existeConexionInternet()){
            alerta.mostrarAlertaSinInternet(targetVC: self)
            return
        }

        
        dataProvider.enviarEmail(usuario: usuario.text!) { isValido  in
            if let isValido = isValido{
                switch isValido{
                case "0":
                    alerta.mostrarAlerta(targetVC: self, titulo: "Usuario no encontrado", mensaje: "")
                case "1":
                    alerta.mostrarAlerta(targetVC: self, titulo: "Contraseña enviada a su cuenta de email  del registro", mensaje: "")


                default:
                    alerta.mostrarAlertaSinServicio(targetVC: self)
                    print ("otro error")

                
            }
        }
            alerta.mostrarAlertaSinServicio(targetVC: self)

            
    }
    }
    
    
    
    @IBAction func enterButton(_ sender: AnyObject) {
        
        
        if (!existeConexionInternet()){
            alerta.mostrarAlertaSinInternet(targetVC: self)
            return
        }

        var  showWaiting = true
        let alert = Alerta()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            if showWaiting{
                alert.iniciarFastLoadingTag(targetVC: self, mensaje: "", tag: 100)
            }
        })

        
//        alert.iniciarLoading(targetVC: self, mensaje: "")
    
        
        dataProvider.validarLogin(usuario: usuario.text!, pass: pass.text!) { usuarioParametros  in
            showWaiting = false
            alert.pararLoadingTag(targetVC: self, tag: 100)


            if let isValido = usuarioParametros?.codigoCtr{
               
                switch isValido{
                    case "0":
                        alerta.mostrarAlerta(targetVC: self, titulo: "Usuario no encontrado", mensaje: "")
//                        alerta.mostrarAlertaSinServicio(targetVC: self)

                    
                    case "1":
//                        familias
//                        alert.iniciarFastLoadingTag(targetVC: self, mensaje: "", tag: 100)
                                currentUser = self.usuario.text!
                                    dataProvider.listaFamilias(usuario: currentUser) { vfamilias in
//                        alert.pararLoadingTag(targetVC: self, tag: 100)
                                    if let vfamilias = vfamilias {
                                            familias = vfamilias
                           // Lista de pedidos
                                        currentUserParametros = usuarioParametros
                                        UserDefaults.standard.set(self.usuario.text! , forKey: "Usuario")
                                        let vc = (self.storyboard?.instantiateViewController(withIdentifier: "listaPedidos"))! as! BrowsePedidosTableViewController
                                        self.navigationController?.pushViewController(vc, animated: true)

                                        
                                    }else{
                                        alert.mostrarAlertaSinServicio(targetVC: self)
                                    }
                                }

                        
                        
                        
                        
//                        UserDefaults.standard.set(self.usuario.text , forKey: "Usuario")
//                      
//                        currentUser = self.usuario.text
//                        let vc = (self.storyboard?.instantiateViewController(withIdentifier: "listaPedidos"))! as! BrowsePedidosTableViewController
//                        self.navigationController?.pushViewController(vc, animated: true)
                    case "2":
                            self.pass.text = ""
                            alerta.mostrarAlerta(targetVC: self, titulo: "Contraseña no válida", mensaje: "")
//                            alerta.mostrarAlertaSinServicio(targetVC: self)


                    default:
                    alerta.mostrarAlertaSinServicio(targetVC: self)
                    print ("otro error")
                    
                }
                
//                if isValido == "0"{
//                    alerta.mostrarAlerta(targetVC: self, titulo: "Error de validación", mensaje: "Entre un email y una contraseña")
//                }else{
//
//                    currentUser = self.usuario.text
//                    let vc = (self.storyboard?.instantiateViewController(withIdentifier: "listaPedidos"))! as! BrowsePedidosTableViewController
//                    self.navigationController?.pushViewController(vc, animated: true)
//                }
//                
//            }else{
//               alerta.mostrarAlertaSinServicio(targetVC: self)
//            }
            
            }else{
                 alert.mostrarAlertaSinServicio(targetVC: self)

            }
            
        }
    
    
    
    
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
   
        if (textField.tag == 1){
            usuario.resignFirstResponder()
            
        }
        if (textField.tag == 2){
            pass.resignFirstResponder()
            
        }
        return true
    }

 
    
    
    
}
