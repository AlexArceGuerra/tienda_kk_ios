//
//  Extensions.swift
//  DemoPedido
//
//  Created by ArceGuerra, Alejandro on 19/10/16.
//  Copyright © 2016 ArceGuerra, Alejandro. All rights reserved.
//

import Foundation




extension String {
    func padding(length: Int) -> String {
        return self.padding(toLength: length, withPad: " " , startingAt: 0)
        
    }
    
    func padding(length: Int, paddingString: String) -> String {
        
        return self.padding(toLength: length, withPad: paddingString, startingAt: 0)
        
    }
    func toBool() -> Bool? {
        switch self {
        case "True", "true", "yes", "1":
            return true
        case "False", "false", "no", "0":
            return false
        default:
            return nil
        }
    }
    
    
    
    
}


func existeConexionInternet() -> Bool {
    
    let networkStatus = Reachability().connectionStatus()
    switch networkStatus {
    case .Unknown, .Offline:
        return false
    case .Online(.WWAN):
        print("Connected via WWAN")
        return true
    case .Online(.WiFi):
        print("Connected via WiFi")
        return true
    }


    


}



