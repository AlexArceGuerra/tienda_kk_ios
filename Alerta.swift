//
//  Alerta.swift
//  DemoPedido
//
//  Created by Alejandro Arce on 25/4/17.
//  Copyright © 2017 ArceGuerra, Alejandro. All rights reserved.
//

import Foundation
import UIKit


class Alerta {
    var alert : UIAlertController!
    
//    func mostrarAlertaConfirmacion(targetVC: UIViewController, titulo: String, mensaje: String){
//        let alert = UIAlertController(title: titulo, message: mensaje, preferredStyle: UIAlertControllerStyle.actionSheet)
//        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//        alert.view.tintColor = UIColor(red: 1.0, green: 0.25, blue: 1.0, alpha: 1.0)
//        
//        
//        var myMutableString = NSMutableAttributedString()
//        var myMutableStringT = NSMutableAttributedString()
//        
//        myMutableString = NSMutableAttributedString(string: titulo as String, attributes: [NSFontAttributeName:UIFont(name: "Helvetica Neue", size: 18.0)!])
//        myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location:0,length:titulo.characters.count))
//        
//        alert.setValue(myMutableString, forKey: "attributedTitle")
//        
//        myMutableStringT = NSMutableAttributedString(string: mensaje as String, attributes: [NSFontAttributeName:UIFont(name: "Helvetica Neue", size: 14.0)!])
//        myMutableStringT.addAttribute(NSForegroundColorAttributeName, value: UIColor.gray, range: NSRange(location:0,length:mensaje.characters.count))
//        
//        alert.setValue(myMutableStringT, forKey: "attributedMessage")
//        
//        
//        
//        
//        targetVC.present(alert, animated: true, completion: nil)
//        
//    }

    func mostrarAlertaAction(targetVC: UIViewController, titulo: String, mensaje: String){
        alert = UIAlertController(title: titulo, message: mensaje, preferredStyle: UIAlertControllerStyle.actionSheet)
//        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        alert.view.tintColor = UIColor(red: 1.0, green: 0.50, blue: 0.0, alpha: 1.0)
        
        
        var myMutableString = NSMutableAttributedString()
        var myMutableStringT = NSMutableAttributedString()
        
        myMutableString = NSMutableAttributedString(string: titulo as String, attributes: [NSFontAttributeName:UIFont(name: "Helvetica Neue", size: 18.0)!])
        myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location:0,length:titulo.characters.count))
        
        alert.setValue(myMutableString, forKey: "attributedTitle")
        
        myMutableStringT = NSMutableAttributedString(string: mensaje as String, attributes: [NSFontAttributeName:UIFont(name: "Helvetica Neue", size: 14.0)!])
        myMutableStringT.addAttribute(NSForegroundColorAttributeName, value: UIColor.gray, range: NSRange(location:0,length:mensaje.characters.count))
        
        alert.setValue(myMutableStringT, forKey: "attributedMessage")
        
        
        
        
        targetVC.present(alert, animated: true, completion: nil)

    
    }
    
    
    func mostrarAlerta(targetVC: UIViewController, titulo: String, mensaje: String){
        
//        mostrarAlerta(targetVC: targetVC, titulo: titulo, mensaje: mensaje)
//        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//
        
        alert = UIAlertController(title: titulo, message: mensaje, preferredStyle: UIAlertControllerStyle.actionSheet)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        alert.view.tintColor = UIColor(red: 1.0, green: 0.50, blue: 0.0, alpha: 1.0)
        
        
        var myMutableString = NSMutableAttributedString()
        var myMutableStringT = NSMutableAttributedString()
        
        myMutableString = NSMutableAttributedString(string: titulo as String, attributes: [NSFontAttributeName:UIFont(name: "Helvetica Neue", size: 18.0)!])
        myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location:0,length:titulo.characters.count))
        
        alert.setValue(myMutableString, forKey: "attributedTitle")
        
        myMutableStringT = NSMutableAttributedString(string: mensaje as String, attributes: [NSFontAttributeName:UIFont(name: "Helvetica Neue", size: 14.0)!])
        myMutableStringT.addAttribute(NSForegroundColorAttributeName, value: UIColor.gray, range: NSRange(location:0,length:mensaje.characters.count))
        
        alert.setValue(myMutableStringT, forKey: "attributedMessage")
        
        
        
        
        targetVC.present(alert, animated: true, completion: nil)
        
    }
    
    func mostrarAlertaSinServicio (targetVC: UIViewController){
        
        mostrarAlerta(targetVC: targetVC, titulo: "El servidor no esta disponible", mensaje: "Inténtalo en unos minutos")
        
        
    }
    
    func mostrarAlertaSinInternet (targetVC: UIViewController){
        
        mostrarAlerta(targetVC: targetVC, titulo: "Sin Acceso a internet", mensaje: "Conecta WiFI/3G/4G para continuar")
        
        
    }
    
    func iniciarLoading(targetVC: UIViewController, mensaje: String){
        
        
        // activity
        let overlay = UIView(frame: targetVC.view.frame)
        overlay.center = targetVC.view.center
        
        overlay.alpha = 0
        overlay.backgroundColor = UIColor.black
        overlay.tag = 100
        targetVC.view.addSubview(overlay)
        targetVC.view.bringSubview(toFront: overlay)
        
        UIView .animate(withDuration: 0.5) {
            UIView.setAnimationDuration(0.5)
            overlay.alpha = 0.8
            
        }
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        indicator.center = overlay.center
        indicator.startAnimating()
        overlay.addSubview(indicator)
        let label = UILabel()
        label.text = mensaje
        label.textColor = UIColor.white
        label.sizeToFit()
        label.center = CGPoint(x: indicator.center.x, y: indicator.center.y + 30)
        overlay.addSubview(label)
        
        //        UIApplication.shared.beginIgnoringInteractionEvents()
        
    }
    
    func iniciarFastLoading(targetVC: UIViewController, mensaje: String){
        
        
        // activity
        let overlay = UIView(frame: targetVC.view.frame)
        overlay.center = targetVC.view.center
        
        overlay.alpha = 0.5
        overlay.backgroundColor = UIColor.black
        overlay.tag = 100
        
        //        UIView .animate(withDuration: 0.0) {
        //            UIView.setAnimationDuration(0.0)
        //            overlay.alpha = 0.8
        //
        //        }
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        indicator.center = overlay.center
        indicator.startAnimating()
        overlay.addSubview(indicator)
        let label = UILabel()
        label.text = mensaje
        label.textColor = UIColor.white
        label.sizeToFit()
        label.center = CGPoint(x: indicator.center.x, y: indicator.center.y + 30)
        overlay.addSubview(label)
        
        targetVC.view.addSubview(overlay)
        targetVC.view.bringSubview(toFront: overlay)

        
        //        UIApplication.shared.beginIgnoringInteractionEvents()
        
    }
    func pararLoading(targetVC: UIViewController){
        
        if let viewWithTag = targetVC.view.viewWithTag(100) {
            viewWithTag.removeFromSuperview()
            
        }
    }
    func iniciarFastLoadingTag(targetVC: UIViewController, mensaje: String, tag: Int){
        
        
        // activity
        let overlay = UIView(frame: targetVC.view.frame)
        overlay.center = targetVC.view.center
        
        overlay.alpha = 0.5
        overlay.backgroundColor = UIColor.black
        overlay.tag = tag
        
        //        UIView .animate(withDuration: 0.0) {
        //            UIView.setAnimationDuration(0.0)
        //            overlay.alpha = 0.8
        //
        //        }
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        indicator.center = overlay.center
        indicator.startAnimating()
        overlay.addSubview(indicator)
        let label = UILabel()
        label.text = mensaje
        label.textColor = UIColor.white
        label.sizeToFit()
        label.center = CGPoint(x: indicator.center.x, y: indicator.center.y + 30)
        overlay.addSubview(label)
        
        targetVC.view.addSubview(overlay)
        targetVC.view.bringSubview(toFront: overlay)
        
        
        //        UIApplication.shared.beginIgnoringInteractionEvents()
        
    }
    func pararLoadingTag(targetVC: UIViewController, tag: Int){
        
        if let viewWithTag = targetVC.view.viewWithTag(tag) {
            viewWithTag.removeFromSuperview()
            
        }
    }

}
