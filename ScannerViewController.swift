//
//  ScannerViewController.swift
//  DemoPedido
//
//  Created by Alejandro Arce on 3/10/16.
//  Copyright © 2016 ArceGuerra, Alejandro. All rights reserved.
//

import AVFoundation
import UIKit
protocol ScannerViewControllerDelegate {
    func scanQRFound(text: String) }

class ScannerViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate,UnitEnterViewControllerDelegate {
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    var delegate : ScannerViewControllerDelegate?
   
    @IBOutlet weak var scanV: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.toolbar.isHidden = true
        self.title = "Scanner"
        
        view.backgroundColor = UIColor.black
        captureSession = AVCaptureSession()
        
        let videoCaptureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed();
            return;
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
//            metadataOutput.metadataObjectTypes = [AVMetadataObjectTypeQRCode]
//            metadataOutput.metadataObjectTypes = [AVMetadataObjectTypeEAN13Code]
            metadataOutput.metadataObjectTypes = metadataOutput.availableMetadataObjectTypes
            
        } else {
            failed()
            return
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession);
        
//        previewLayer.frame = view.layer.bounds;
//      previewLayer.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height-50)
        
        
        previewLayer.frame = scanV.frame

        previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        view.layer.addSublayer(previewLayer);
        
        
        
        
        captureSession.startRunning();
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
    }
    
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (captureSession?.isRunning == false) {
            captureSession.startRunning();
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning();
        }
    }
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
//        captureSession.stopRunning()
        
        if let metadataObject = metadataObjects.first {
            print ( metadataObject)
            if metadataObject is AVMetadataMachineReadableCodeObject{
                captureSession.stopRunning()
                AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
               // cargarArticulos()
                
                found(code: (metadataObject as AnyObject).stringValue);

            }
            
//            let readableObject = metadataObject as! AVMetadataMachineReadableCodeObject
//            
//            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
//            found(code: readableObject.stringValue);
//
            
            
        }else{
        
            navigationController?.popViewController(animated: true)
        }
    }
    
    func found(code: String) {
    
   //  currentUser = "344440"
    // code = "18411530101348"
        
      //  18411530101348
        
        // "8422088900966"
        
        
        dataProvider.buscarArticulo(usuario: currentUser!, cod_articulo: "", ref_art_cliente: "", cod_familia: "", codigo_EAN_DUN: code, des_articulo: "", novedad: "") { articulos in
            
            alerta.pararLoading(targetVC: self)
            if let articulos = articulos {
                if articulos.count > 0 {
                    
//                    let articleSeleted = articles[0]
//                    // unit call
//                    let vc = (self.storyboard?.instantiateViewController(withIdentifier: "UnitEnter"))! as! UnitEnterViewController
//                    vc.delegate = self
//                    self.present(vc, animated: true, completion: nil)
//                   vc.updateOrderDetailFromArticle(article: articleSeleted, deleteAllowed: false)

            
                    articles = articulos
                    let vc = (self.storyboard?.instantiateViewController(withIdentifier: "listaArticulos"))! as! ListaArticulosTableViewController
                    
                    self.navigationController?.pushViewController(vc, animated: true)

                
                
                
                
                }else{
                let alertController = UIAlertController(title: code, message:
                    " ! No encontrado en almacen! ", preferredStyle: UIAlertControllerStyle.actionSheet)
                alertController.addAction(UIAlertAction(title: "Buscar otro",
                                                        style: UIAlertActionStyle.default,
                                                        handler: {(alert: UIAlertAction!) in
                                                            self.captureSession.startRunning();
                }))

                self.present(alertController, animated: true, completion: nil)
                }

            }else{
                
                let alertController = UIAlertController(title: code, message:
                    " ! No encontrado ! ", preferredStyle: UIAlertControllerStyle.actionSheet)
                alertController.addAction(UIAlertAction(title: "Buscar otro",
                                              style: UIAlertActionStyle.default,
                                              handler: {(alert: UIAlertAction!) in
                                                self.captureSession.startRunning();
                }))
                alertController.addAction(UIAlertAction(title: "Cancelar",
                                                        style: UIAlertActionStyle.default,
                                                        handler: {(alert: UIAlertAction!) in
                                                            self.dismiss(animated: true, completion: nil)
                                                            self.navigationController?.popViewController(animated: true)

                                                            
                }))

                self.present(alertController, animated: true, completion: nil)

            
            }
            
        }

        
        
        
        
        print(code)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }

    // MARK: delegate UnitEnter
    func unitEnterWaschanged() {
        self.captureSession.startRunning()
        
    }

    
    // to delete
    
    func cargarArticulos(){
        
        
        alerta.iniciarFastLoading(targetVC: self, mensaje: "")
        
      //  dataProvider.buscarArticulo(usuario: currentUser!, cod_articulo: "", ref_art_cliente: "", cod_familia: "", codigo_EAN_DUN: "", des_articulo: "", novedad: "0") { articulos in

        
        dataProvider.buscarArticulo(usuario: "344440", cod_articulo: "", ref_art_cliente: "", cod_familia: "", codigo_EAN_DUN: "8422088900966", des_articulo: "", novedad: "") { articulos in
            
            alerta.pararLoading(targetVC: self)
            if let articulos = articulos {
                if articulos.count < 1 {
                    // sin resultados
                    let alertController = UIAlertController(title: "Sin resultados", message: "Pruebe con otra selección ", preferredStyle: .actionSheet)
                    alertController.view.tintColor = UIColor(red: 1.0, green: 0.50, blue: 0.0, alpha: 1.0)
                    
                    let oKAction = UIAlertAction(title: "OK", style: .default, handler: {
                        alert -> Void in
//                        self.ponerEnBlanco(all: true)
                        
                    })
                    alertController.addAction(oKAction)
                    self.present(alertController, animated: true, completion: nil)
                    //                    alerta.mostrarAlerta(targetVC: self, titulo: "Sin resultados", mensaje:"Pruebe con otra selección ")
                    //                   return
                }else{
                    // artículos encontrados en la lista
                 //  self.dismiss(animated: true, completion: nil)
                    articles = articulos
                    let vc = (self.storyboard?.instantiateViewController(withIdentifier: "listaArticulos"))! as! ListaArticulosTableViewController
                    //        vc.delegate = self
                    //self.present(vc, animated: true, completion: nil)

                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }
            }else{
                // sin servicio
                alerta.mostrarAlertaSinServicio(targetVC: self)
            }
            
        }
        
    }
    
    
    
}
