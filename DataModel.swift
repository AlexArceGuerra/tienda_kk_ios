//
//  DataModel.swift
//  DemoPedido
//
//  Created by ArceGuerra, Alejandro on 19/10/16.
//  Copyright © 2016 ArceGuerra, Alejandro. All rights reserved.
//

import Foundation
struct lineaPedido {
    
    var descripcion = String()
    let pack:Int
    var precio:Float
    var unidades:Int
    var importe:Float
    
}

var arrayLineaPedido = [lineaPedido]()



//new
import Foundation

//struct  Pedido {
//    var  fecha : String
//    var numPedido : String
//    var estado :String
//    var numPedidoWeb : String
//    init(fecha: String, numPedido: String, estado: String, numPedidoWeb : String) {
//        self.fecha = fecha
//        self.numPedido = numPedido
//        self.estado = estado
//        self.numPedidoWeb = numPedidoWeb
//    }
//}
//struct  Pedido {
//    var  fecha : String
//    var numPedido : String
//    var estado :String
//    var numPedidoWeb : String
//    init(fecha: String, numPedido: String, estado: String, numPedidoWeb : String) {
//        self.fecha = fecha
//        self.numPedido = numPedido
//        self.estado = estado
//        self.numPedidoWeb = numPedidoWeb
//    }
//}



struct UsuarioParametros{
    var ver_precios : Bool
    var desc_usuario : String
    var permite_scan : Bool
    var codigoCtr : String
    init(ver_precios : String, desc_usuario : String, permite_scan : String, codigoCtr : String) {
        self.ver_precios = ver_precios.toBool()!
        self.desc_usuario = desc_usuario
        self.permite_scan = permite_scan.toBool()!
        self.codigoCtr = codigoCtr
    }
}




struct Familia {
    var cod_fam_sub : String
    var desc_fam_sub : String
    init(cod_fam_sub : String, desc_fam_sub : String) {
        self.cod_fam_sub = cod_fam_sub
        self.desc_fam_sub = desc_fam_sub
    }
}


struct Article {
    var des_articulo : String
    var cod_articulo : String
    let pack : Int
    var precio_pack: Double
    var peso: Double
    var novedad : String
    var tipo_iva: Double
    var tamanio: String

    init(des_articulo : String, cod_articulo : String, pack : String, precio_pack: String, peso: String, novedad : String,tipo_iva: String,tamanio: String) {
        self.des_articulo = des_articulo
        self.cod_articulo = cod_articulo
        self.pack = Int(pack)!
        self.precio_pack = Double(precio_pack)!
        self.peso = Double(peso)!
        self.novedad = novedad
        self.tipo_iva = Double(tipo_iva)!
        self.tamanio = tamanio

    }
    
}

struct OrderHead{
    var  fecha : String
    var numPedido : String
    var estado : Bool
    var numPedidoWeb : String
    init(fecha: String, numPedido: String, estado: String, numPedidoWeb : String) {
        self.fecha = fecha
        self.numPedido = numPedido
        if estado == "N"{
            self.estado = true
        }else{
            self.estado = false
        }

        self.numPedidoWeb = numPedidoWeb
    }
}

struct OrderDetail {
    var des_articulo : String
    var cod_articulo : String
    var ref_cliente : String
    let pack : Int
    var precio_pack: Double
    var peso: Double
    var cantidad : Int
    var num_linea :Int
    var novedad : String
    var tipo_iva: Double
    init(des_articulo : String, cod_articulo : String, ref_cliente : String ,  pack : String, precio_pack: String, peso: String, cantidad : String, num_linea : String, novedad : String,tipo_iva: String) {
        self.des_articulo = des_articulo
        self.cod_articulo = cod_articulo
        self.ref_cliente = ref_cliente
        self.pack = Int(pack)!
        self.precio_pack = Double(precio_pack)!
        self.peso = Double(peso)!
        self.cantidad = Int(cantidad)!
        self.num_linea = Int(num_linea)!
        self.novedad = novedad
        self.tipo_iva = Double(tipo_iva)!
    }
}
struct UpdateDetailResult {
    var codigo_ctr : String
    var num_lin  : String
    init(codigo_ctr : String,num_lin  : String){
        self.codigo_ctr = codigo_ctr
        self.num_lin = num_lin
        
    }

}

func searchCodigoArticulo(cod: String) -> Article{
    
    let article : Article = Article(des_articulo: "", cod_articulo: "", pack: "", precio_pack: "", peso: "", novedad: "", tipo_iva: "", tamanio: "")
    //    for art in articles {
//        if ( art.code == cod ){
//            return art
//        }
//    }
//    return Article(code: "", ref: "", description: "", packUnit: 0, priceUnit: 0)
    return article
}
