//
//  Variables.swift
//  DemoPedido
//
//  Created by Alejandro Arce on 27/4/17.
//  Copyright © 2017 ArceGuerra, Alejandro. All rights reserved.
//

import Foundation

let alerta = Alerta()
let dataProvider = ConnectionRemoteService()
var currentUser : String!
var currentUserParametros : UsuarioParametros!
var currentOrder: OrderHead!
var currentOrderDetail : OrderDetail!


var familias : [Familia] = []
var articles: [Article] = []
var ordersHead: [OrderHead] = []
var orderDetails: [OrderDetail] = []
